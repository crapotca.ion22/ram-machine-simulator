// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"), require("../../mode/ram_machine/ram_machine"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror", "../../mode/ram_machine/ram_machine"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("hint", "ram_machine", function(cm) {
    var cur = cm.getCursor(), token = cm.getTokenAt(cur);
    var inner = CodeMirror.innerMode(cm.getMode(), token.state);
    if (inner.mode.name != "ram_machine") return;

    var start = token.start;
    var end = cur.ch;
    var word = token.string.slice(0, end - start).trim();

    if (word.length)
    {
      if (!inner.state.nextTokenType || inner.state.nextTokenType === "opcode")
      {
        var hints = [];
        $.each(inner.mode.opCodesList, function() {
          if (this.startsWith(word.toUpperCase()) && hints.indexOf(this) < 0)
            hints.push(this);
        });
  
        $.each(inner.state.usedLabels, function() {
          if (this.startsWith(word) && hints.indexOf(this) < 0 && inner.state.definedLabels.indexOf(this) < 0)
            hints.push(this);
        });
        return {list: hints, from: CodeMirror.Pos(cur.line, token.start), to: CodeMirror.Pos(cur.line, token.end)};
      }
  
      if (token.type != "error" && inner.state.lastTokenType === "label" && inner.state.nextTokenType === "none")
      {
        var hints = [];
        $.each(inner.state.definedLabels, function() {
          if (this.startsWith(word) && hints.indexOf(this) < 0)
            hints.push(this);
        });
        return {list: hints, from: CodeMirror.Pos(cur.line, token.start), to: CodeMirror.Pos(cur.line, token.end)};
      }
    }

  });
});
