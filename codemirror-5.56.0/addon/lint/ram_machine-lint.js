// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
  "use strict";

  CodeMirror.registerHelper("lint", "ram_machine", function(text, options){
    var result = [];
    if (options.editor) {
      var inner = CodeMirror.innerMode(options.editor.getMode());
      if (inner.mode.name == "ram_machine")
      {
        var totLines = options.editor.lineCount();
        for (var x = 0; x < totLines; x++)
        {
          var message, severity;
          var lineTokens = getLineTokens(options.editor, x);
          var lastToken = null;
          $.each(lineTokens, function() {
            if (this.type != "comment")
            {
              if (this.type == "label-not-used")
              {
                severity = "warning";
                message = 'The label "' + this.string + '" is never used.';
                result.push(lint(x, this.start, this.end, message, severity));
              }            
              if (this.type == "label-not-defined")
              {
                severity = "error";
                message = 'The label "' + this.string + '" is not defined.';
                result.push(lint(x, this.start, this.end, message, severity));
              }
              if (this.type == "label-redefined")
              {
                severity = "error";
                message = 'The label "' + this.string + '" is defined more than once.';
                result.push(lint(x, this.start, this.end, message, severity));
              }
  
              if (this.type == "error")
              {
                severity = "error";
                if (this.state.nextTokenType == "opcode" || this.state.nextTokenType == "operand")
                {
                  message = 'Expected an ' + this.state.nextTokenType;
                  if (this.state.nextTokenType == "operand")
                  {
                    message += ' of type "i"';
  
                    if (!(this.string.startsWith("=") && inner.mode.noDirectOpCodes.indexOf(this.state.lastOpCode) > -1))
                      message += ', "=i"';
  
                    message += ' or "*i"'
                  }
                  message += ' and instead saw "' + this.string + '".';
                }
  
                if (this.state.nextTokenType == "none")
                  message = 'Expected nothing and instead saw "' + this.string + '".';
                  
                result.push(lint(x, this.start, this.end, message, severity));
              }
              
              if (this.type && (this.type.startsWith("label") || this.type.startsWith("op")))
                lastToken = this;
            }
          });
  
          if (lastToken)
          {
            if (lastToken.state.nextTokenType != "none" && lastToken.state.lastTokenType != "label")
            {
              severity = "error";
              message = 'Missing ' + lastToken.state.nextTokenType + '.';
  
              var start = lastToken.end;
              var end = options.editor.getLine(x).length;
              var pos = {
                line: x,
                ch: start
              }
  
              if (start == end && options.editor.getCursor().line != pos.line && options.editor.getCursor().ch != pos.ch)
              {
                end++;
                options.editor.replaceRange(" ", pos);
              }
  
              result.push(lint(x, start, end, message, severity));
            }
          }
        }
      }
    }
    return result;
  });

  function getLineTokens(editor, line) {
    var lineTokens = [];
    var lastToken = null;

    $.each(editor.getLineTokens(line), function() {
      var thisToken = this;
      if (lastToken)
      {
        if (lastToken.type == "error" && thisToken.type == "error")
        {
          thisToken.string = lastToken.string + thisToken.string;
          thisToken.start = lastToken.start;
        }

        if (lastToken.type != "error" || thisToken.type != "error")
          lineTokens.push(lastToken);
      }
      lastToken = thisToken;
    });

    if (lastToken)
      lineTokens.push(lastToken);

    return lineTokens;
  }

  function lint(line, start, end, message, severity) {
    return {
      message: message,
      severity: severity,
      from: {
        line: line,
        ch: start,
        sticky: null
      },
      to: {
        line: line,
        ch: end,
        sticky: null
      }
    };
  }

});
