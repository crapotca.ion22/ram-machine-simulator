// CodeMirror, copyright (c) by Marijn Haverbeke and others
// Distributed under an MIT license: https://codemirror.net/LICENSE

(function(mod) {
  if (typeof exports == "object" && typeof module == "object") // CommonJS
    mod(require("../../lib/codemirror"));
  else if (typeof define == "function" && define.amd) // AMD
    define(["../../lib/codemirror"], mod);
  else // Plain browser env
    mod(CodeMirror);
})(function(CodeMirror) {
"use strict";

CodeMirror.defineMode("ram_machine", function(_config, parserConfig) {
  function wordRegexp(words, end) {
    if (typeof end == "undefined") { end = "\\b"; }
    return new RegExp("^((" + words.join(")|(") + "))" + end);
  }

  var registryOpCodes = ["LOAD", "STORE"];
  var arithmeticOpCodes = ["ADD", "SUB", "MULT", "DIV"];
  var ioOpCodes = ["READ", "WRITE"];
  var jumpOpCodes = ["JUMP", "JGTZ", "JZERO", "JBLANK"];
  var stopOpCodes = ["HALT"];
  
  var noDirectOpCodes = ["STORE", "READ"];

  var opCodesList = registryOpCodes.concat(arithmeticOpCodes).concat(ioOpCodes).concat(jumpOpCodes).concat(stopOpCodes);
  
  function checkMultilineComment(stream, state) {
    var maybeEnd = false, ch;
    while ((ch = stream.next()) != null) {
      if (ch == "/" && maybeEnd) {
        state.tokenize = null;
        break;
      }
      maybeEnd = (ch == "*");
    }
    return "comment";
  }
  
  function isLabelAlreadyDefined(label, stream, state) {
    if (state.definedLabels.indexOf(label) > -1)
      return true;
    
    var nextText = stream.string.replace(stream.current(), "").trim();

    var linesAhead = 1;
    var line;
    while (typeof (line = stream.lookAhead(linesAhead)) !== "undefined")
    {
      linesAhead++;
      nextText += "\n" + line;
    }

    nextText = nextText.replace(/\/\*[\s\S]*?\*\/|\/\/.*/g,'');

    var firstLine = true;
    var foundLabel = false;
    $.each(nextText.split("\n"), function() {
      if (!firstLine) {
        var tokens = this.match(/\S+/g);
        if (tokens != null && tokens[0] == label)
        {
          foundLabel = true;
          return false;
        }
      }
      else
        firstLine = false;
    });

    return foundLabel;
  }
  
  function isLabelAlreadyUsed(label, stream, state) {
    if (state.usedLabels.indexOf(label) > -1)
      return true;
    
    var nextText = stream.string.replace(stream.current(), "").trim();

    var linesAhead = 1;
    var line;
    while (typeof (line = stream.lookAhead(linesAhead)) !== "undefined")
    {
      linesAhead++;
      nextText += "\n" + line;
    }

    nextText = nextText.replace(/\/\*[\s\S]*?\*\/|\/\/.*/g,'');

    var labelUsed = false;
    $.each(nextText.split("\n"), function() {
      var tokens = this.match(/\S+/g);

      var foundJump = false;
      $.each(tokens, function() {
        if (foundJump)
        {
          labelUsed = (this == label);
          return false;
        }
        foundJump = (jumpOpCodes.indexOf(this.toUpperCase()) > -1);
      });

      if (labelUsed)
        return false;
    });

    return labelUsed;
  }
  
  return {
    startState: function() {
      return {
        tokenize: null,
        lastOpCode: null,
        lastToken: null,
        lastTokenType: null,
        nextTokenType: null,
        definedLabels: [],
        usedLabels: []
      };
    },

    token: function(stream, state) {
      if (state.tokenize) {
        return state.tokenize(stream, state);
      }
      
      if (stream.sol()) {
        state.lastOpCode = null;
        state.lastToken = null;
        state.lastTokenType = null;
        state.nextTokenType = null;
      }

      if (stream.eatSpace()) {
        return null;
      }

      var ch = stream.next();

      if (/\S/.test(ch)) {
        if (stream.eat("/")) {
          stream.skipToEnd();
          state.tokenize = null;
          return "comment";
        }

        if (stream.eat("*")) {
          state.tokenize = checkMultilineComment;
          return checkMultilineComment(stream, state);
        }

        stream.eatWhile(/\S/);

        var cur = String(stream.current());
        state.lastToken = cur.toUpperCase();

        if (!state.nextTokenType || state.nextTokenType == "opcode")
        {
          if (opCodesList.indexOf(state.lastToken) > -1)
          {
            state.lastTokenType = "opcode";

            state.nextTokenType = "operand";
            if (jumpOpCodes.indexOf(state.lastToken) > -1)
              state.nextTokenType = "label";
            if (stopOpCodes.indexOf(state.lastToken) > -1)
              state.nextTokenType = "none";
              
            state.lastOpCode = state.lastToken;
            return 'opcode';
          }

          /*
          var maybeOpCode = false;
          $.each(opCodesList, function() {
            var word = cur;
            var nextChar = stream.peek();
            if (nextChar)
              word += nextChar;
            if (this.startsWith(word.toUpperCase()))
            {
              maybeOpCode = true;
              return false;
            }
          });
          if (maybeOpCode)
            return 'opcode';
          */

          if (!state.nextTokenType)
          {
            state.lastTokenType = "label";
            state.nextTokenType = "opcode";

            if (isLabelAlreadyDefined(cur, stream, state)) {
              state.definedLabels.push(cur);
              return 'label-redefined';
            }
            
            state.definedLabels.push(cur);

            if (!isLabelAlreadyUsed(cur, stream, state))
              return 'label-not-used';
              
            return 'label';
          }
        }
        
        if (state.nextTokenType == "operand")
        {
          stream.backUp(cur.length);
          ch = stream.next();

          if (ch == "=") {
            //if (stream.eatWhile(/-?\d+/)) {
            if (stream.eatWhile(/\d/) || stream.eat(/-?/) && stream.eatWhile(/\d/)) {
              if (noDirectOpCodes.indexOf(state.lastOpCode) > -1)
                return 'error';
                
              state.lastTokenType = "operand";
              state.nextTokenType = "none";
              return "operand";
            }
            /*
            if (!stream.peek() && typeof stream.lookAhead(1) === "undefined")
              return "operand";
            */
          }
    
          if (/\d/.test(ch)) {
            stream.eatWhile(/\d/);
            state.lastTokenType = "operand";
            state.nextTokenType = "none";
            return "operand-2";
          }
          
          if (ch == "*") {
            if (stream.eatWhile(/\d/)) {
              state.lastTokenType = "operand";
              state.nextTokenType = "none";
              return "operand-3";
            }
            /*
            if (!stream.peek() && typeof stream.lookAhead(1) === "undefined")
              return "operand-3";
            */
          }
        }
        
        if (state.nextTokenType == "label")
        {
          if (/\S/.test(ch)) {
            stream.eatWhile(/\S/);

            if (opCodesList.indexOf(state.lastToken) > -1)
              return 'error';

            state.usedLabels.push(cur);
            state.lastTokenType = "label";
            state.nextTokenType = "none";

            if (!isLabelAlreadyDefined(cur, stream, state))
              return 'label-not-defined';
              
            return "label";
          }          
        }
      }
      
      return "error";
    },

    opCodesList: opCodesList,
    noDirectOpCodes: noDirectOpCodes
  };
});

});
