function initializeCost(register) {
  var cost = {
    m_register: null,
    m_input: null,
    m_bIsAborted: false,
    m_timeUni: $("#cost-time-uniform"),
    m_timeLog: $("#cost-time-logarithmic"),
    m_spaceUni: $("#cost-space-uniform"),
    m_spaceLog: $("#cost-space-logarithmic"),
    m_instructionsCount: 0,
    m_spaceLogMax: 0,
    m_timeLogCounter: 0,
    m_usedRegisters: new Map(),
    initialize: function() {
      this.bindEvents();
      this.resetCost();
    },
    bindEvents: function() {
    },
    passRegister: function(register) {
      this.m_register = register;
    },
    passInput: function(input) {
      this.m_input = input;
    },
    resetCost: function() {
      this.m_timeUni.html("?");
      this.m_timeLog.html("?");
      this.m_spaceUni.html("?");
      this.m_spaceLog.html("?");
      this.m_instructionsCount = 0;
      this.m_timeLogCounter = 0;
      this.m_spaceLogMax = 0;
      this.m_usedRegisters = new Map();
      this.m_bIsAborted = false;
    },
    isJumpOpCode: function(opCode) {
      var jumpOpCodes = ["JUMP", "JGTZ", "JZERO", "JBLANK"];
      return (jumpOpCodes.indexOf(opCode) > -1); 
    },
    isIoOpCode: function(opCode) {
      var ioOpCodes = ["READ", "WRITE"];
      return (ioOpCodes.indexOf(opCode) > -1); 
    },
    updateUsedRegisters: function(register) {
      register = parseInt(register);
      if (!this.m_usedRegisters.has(register))
        this.m_usedRegisters.set(register, "");
    },
    getValueLength: function(value) {
      var length = (value == 0) ? 0 : Math.floor(Math.log2(Math.abs(value)));
      return length +1;
    },
    update: function(instruction) {
      if (this.m_bIsAborted)
        return;
      
      this.m_instructionsCount++;
      
      var operand = instruction.operand;
      if (operand != null && !this.isJumpOpCode(instruction.opcode))
      {
        if (!this.isIoOpCode(instruction.opcode))
          this.updateUsedRegisters(0);

        if (isIndirectOperandValue(operand)) {
          operand = cleanOperandValue(operand);
          this.updateUsedRegisters(operand);
            
          operand = this.m_register.getValue(operand)
        }

        if (!isDirectOperandValue(operand))
          this.updateUsedRegisters(operand);
      }

      this.m_timeUni.html(this.m_instructionsCount);
      this.m_spaceUni.html(this.m_usedRegisters.size);

      this.updateLog(instruction);
    },
    updateLog: function(instruction) {
      var count = 0;
      var operand = instruction.operand;
      var opCode = instruction.opcode;
      
      var singleOpCodes = ["JUMP", "JBLANK", "HALT"];
      if (singleOpCodes.indexOf(opCode) > -1)
      {
        count = 1; 
      }
      else
      {
        var jumpCompareOpCodes = ["JGTZ", "JZERO"];
        var useZeroOpCodes = jumpCompareOpCodes.concat(["STORE", "ADD", "SUB", "DIV", "MULT"]);
        if (useZeroOpCodes.indexOf(opCode) > -1)
          count = this.getValueLength(this.m_register.getValue(0));
        if (opCode == "READ")
          count = this.getValueLength(this.m_input.getPrev());
        
        if (jumpCompareOpCodes.indexOf(opCode) < 0)
        {
          if (isIndirectOperandValue(operand))
          {
            operand = cleanOperandValue(operand);
            count += this.getValueLength(operand);
            operand = this.m_register.getValue(operand);
          }
          if (!isDirectOperandValue(operand))
          {
            count += this.getValueLength(operand);
            operand = this.m_register.getValue(operand);
          }
          else
          {
            operand = cleanOperandValue(operand);
          }
          
          var noDirectOpCodes = ["STORE", "READ"];
          if (noDirectOpCodes.indexOf(opCode) < 0)
            count += this.getValueLength(operand);
        }
      }
      
      this.m_timeLogCounter += count;
      this.m_timeLog.html(this.m_timeLogCounter);

      
      if (singleOpCodes.indexOf(opCode) < 0)
      {
        if (count > this.m_spaceLogMax)
          this.m_spaceLogMax = count;
      }
      this.m_spaceLog.html(this.m_spaceLogMax);
    },
    infinite: function() {
      this.m_timeUni.empty().append('<i class="fas fa-infinity"></i>');
      this.m_timeLog.empty().append('<i class="fas fa-infinity"></i>');
      this.m_spaceUni.empty().append('<i class="fas fa-minus"></i>');
      this.m_spaceLog.empty().append('<i class="fas fa-minus"></i>');
      this.m_bIsAborted = true;
    }
  };

  cost.initialize();
  return cost;
}
