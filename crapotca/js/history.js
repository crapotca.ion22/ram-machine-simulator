function initializeHistory() {
  var history = {
    m_body: $("#history-table-body"),
    m_switchChanges: $("#switchChanges"),
    initialize: function() {
      this.bindEvents();
      this.resetHistory();
      
      var showChanges = Cookies.get('showChanges');
      if (showChanges === undefined)
        showChanges = 'true';

      this.m_switchChanges.prop("checked", (showChanges === 'true'));
      this.hideShowChanges();
    },
    bindEvents: function() {
      this.m_switchChanges.on('click', this.onSwitchClick.bind(this));
    },
    onSwitchClick: function() {
      this.hideShowChanges();
    },
    hideShowChanges: function() {
      var showChanges = this.m_switchChanges.is(":checked");
      
      if (showChanges)
        this.m_body.parent().find('.changes').show();
      else
        this.m_body.parent().find('.changes').hide();
        
      Cookies.set('showChanges', showChanges, { expires: 365, sameSite: 'strict' });
    },
    resetHistory: function() {
      this.m_body.empty();
    },
    getCount: function() {
      return this.m_body.find("tr").length;
    },
    createNewValue: function(instruction) {
      var operandClass = "history-operand-1";
      if (instruction.operand != null) {
        if (isDirectOperandValue(instruction.operand))
          operandClass = "history-operand";
        if (isIndirectOperandValue(instruction.operand))
          operandClass = "history-operand-2";
          
        var operandsJump = ["JUMP", "JGTZ", "JZERO", "JBLANK"];
        if (operandsJump.indexOf(instruction.opcode) > -1)
          operandClass = "history-label";
      }

      return $('<tr />')
        .append(
          $('<td />').append(
            $('<span />').text(instruction.line +1)
          ))
        .append(
          $('<td />').append(
            $('<span />').text(instruction.label).addClass("history-label")
          ))
        .append(
          $('<td />').append(
            $('<span />').text(instruction.opcode).addClass("history-opcode")
          ))
        .append(
          $('<td />').append(
            $('<span />').text(instruction.operand).addClass(operandClass)
          ))
        .append(
          $('<td class="changes" />').append(
            
          ));
    },
    addIndirectRegister: function(oldIndex, oldValue, newIndex, newValue) {
      oldIndex = "R" + oldIndex;
      newIndex = "R" + newIndex;
      this.createChange(oldIndex, oldValue, 'used-register-cell-value-indirect', newIndex, newValue, '');
    },
    addRegisterChange: function(index, oldValue, newValue) {
      index = "R" + index;
      this.createChange(index, oldValue, '', index, newValue, 'used-register-cell-value', true);
    },
    addLocationCounterChange: function(oldValue, newValue) {
      var index = "LC";
      this.createChange(index, oldValue, '', index, newValue, 'used-location-counter-value');
    },
    addReadCounterChange: function(oldValue, newValue) {
      var index = "R";
      this.createChange(index, oldValue, '', index, newValue, 'used-read-counter-value');
    },
    addWriteCounterChange: function(oldValue, newValue) {
      var index = "W";
      this.createChange(index, oldValue, '', index, newValue, 'used-write-counter-value');
    },
    createChange: function(oldIndex, oldValue, oldHighlight, newIndex, newValue, newHighlight, append = false) {
      var pointerSkin = "";
      switch (newHighlight) {
        case 'used-location-counter-value':
          pointerSkin = 'current-location-pointer';
          break;
        case 'used-read-counter-value':
          pointerSkin = 'current-read-pointer';
          break;
        case 'used-write-counter-value':
          pointerSkin = 'current-write-pointer';
          break;
      }
      
      var row = $('<div class="row justify-content-center" />').append(
        $('<div class="col-sm-8" />').append(
          $('<div class="row mb-2 justify-content-center" />').append(
            $('<div class="col-sm-4" />').append(
              $('<div class="counter-container '+ pointerSkin +'" />').append(
                $('<input class="counter-value form-control '+ oldHighlight +'" type="text" readonly />').val(oldValue)
              ).append(
                $('<span class="counter-index badge badge-dark '+ oldHighlight.replace('value', 'index') +' translate" />').text(oldIndex)
              )
            )
          )
          .append(
            $('<div class="col-sm-2 history-change-arrow-container" />').append(
              $('<i class="fas fa-long-arrow-alt-right" />')
            )
          )
          .append(
            $('<div class="col-sm-4" />').append(
              $('<div class="counter-container '+ pointerSkin +'" />').append(
                $('<input class="counter-value form-control '+ newHighlight +'" type="text" readonly />').val(newValue)
              ).append(
                $('<span class="counter-index badge badge-dark '+ newHighlight.replace('value', 'index') +' translate" />').text(newIndex)
              )
            )
          )
        )
      );

      this.addChange(row, append);
    },
    addChange: function(change, append = false) {
      if (append)
        this.m_body.find('.changes').last().append(change);
      else
        this.m_body.find('.changes').last().prepend(change);
      this.hideShowChanges();
    },
    newValue: function(instruction) {
      this.m_body.append(this.createNewValue(instruction));
    },
  };

  history.initialize();
  return history;
}
