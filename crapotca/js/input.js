function initializeInput() {
  var input = {
    DOWNLOAD_FILE_NAME: "input.txt",
    INITIAL_REGISTERS: 10,
    m_currentIndex: 1,
    m_bIsEditing: false,
    m_body: $("#input-body"),
    m_btnModify: $("#btn-input-start-edit"),
    m_btnClean: $("#btn-input-clean"),
    m_btnDownload: $("#btn-input-download"),
    m_btnEditSave: $("#btn-input-save-edit"),
    m_btnEditAbort: $("#btn-input-abort-edit"),
    m_btnDoUpload: $("#btn-input-upload-confirm"),
    m_list: $("#edit-input-list"),
    initialize: function() {
      this.bindEvents();
      this.resetInput();
      this.createInitialEmptyCells();
      
      $('.todo-list').sortable({
        placeholder: 'sort-highlight',
        handle: '.handle',
        forcePlaceholderSize: true
      });

      this.m_list.hide();

      $("#edit-input-buttons-container").hide();
    },
    bindEvents: function() {
      this.m_btnDownload.on('click', this.onDownload.bind(this));
      this.m_btnClean.on('click', this.onClean.bind(this));
      this.m_btnDownload.on('click', this.onDownload.bind(this));
      this.m_btnModify.on('click', this.onModify.bind(this));
      this.m_btnEditSave.on('click', this.onEditSave.bind(this));
      this.m_btnEditAbort.on('click', this.onEditAbort.bind(this));
      this.m_btnDoUpload.on('click', this.onDoUpload.bind(this));
    },
    resetInput: function() {
      this.m_currentIndex = 1;

      this.unselectCells();
      this.enableEditing();
      this.cleanPointer();
    },
    createInitialEmptyCells() {
      this.m_body.find(".input-index").parent().remove();
      for (var x = this.m_currentIndex; x < this.m_currentIndex + this.INITIAL_REGISTERS; x++) {
        this.createEmptyCellAt(x);
      }
    },
    createEmptyCellAt: function(index) {
      var newEmpty = this.createNewCell(index, "", true);
      newEmpty.appendTo(this.m_body);
    },
    isEditing: function() {
      return this.m_bIsEditing;
    },
    enableEditing: function() {
      $.each(this.m_body.find(".input-value:visible").not('.blank-cell-value'), function() {
        $(this).prop("readonly", false).removeClass("input-value-disable");
      });

      this.fixNewButton();
    },
    disableEditing: function() {
      $.each(this.m_body.find(".input-value:visible").not('.blank-cell-value'), function() {
        $(this).prop("readonly", true).addClass("input-value-disable");
      });

      this.removeNewButton();
    },
    fixTools: function() {
      this.m_btnClean.prop("disabled", !this.m_body.find(".input-value:visible").not('.blank-cell-value').length && !this.m_list.children().length);
      this.m_btnModify.prop("disabled", !this.m_body.find(".input-value:visible").not('.blank-cell-value').length);
      this.m_btnDownload.prop("disabled", !this.m_body.find(".input-value:visible").not('.blank-cell-value').length);
    },
    onClean: function() {
      this.showLoading();
      if (this.m_list.children().length)
        this.m_list.empty();
      else
        this.createInitialEmptyCells();
      this.fixNewButton();
      this.fixTools();
      this.hideLoading();
    },
    onDownload: function() {
      var values = [];
      $.each(this.m_body.find(".input-value:visible").not('.blank-cell-value'), function() {
        values.push($(this).val());
      });

      downloadFile(this.DOWNLOAD_FILE_NAME, values.join("\n"));
    },
    fixValue: function(value) {
      value = parseInt(value);
      if (isNaN(value))
        value = 0;
      return value;
    },
    fixAllValues: function() {
      var inputObj = this;
      $.each(this.m_body.find(".input-value:visible").not('.blank-cell-value'), function() {
        $(this).val(inputObj.fixValue($(this).val()));
      });
    },
    onEditSave: function() {
      this.stopValuesEditing(true);
    },
    onEditAbort: function() {
      this.stopValuesEditing();
    },
    onNewValue: function() {
      this.newValue(0, true);
    },
    createNewCellInput: function(index, value, isEmpty) {
      var input = $('<input class="input-value form-control mb-2" type="text" />').attr("id", "input-value-" + index).val(this.fixValue(value));      
      if (isEmpty)
        input.addClass("blank-cell-value").addClass("translate").val("Empty");

      return input;
    },
    createNewCell: function(index, value, isEmpty = false) {
      var cell = $('<div class="row justify-content-center" />').append(
        $('<div class="col-sm-7" />').append(
          $('<div class="input-container" />').append(
            this.createNewCellInput(index, value, isEmpty)
          ).append(
            $('<span class="input-index badge badge-dark" />').attr("id", "input-index-" + index).text(index)
          )
        )
      );
              
      if (isEmpty)
        cell.find(".input-index").addClass("blank-cell-index");
      
      return cell;
    },
    cleanPointer: function() {
      this.m_body.find('.current-read-pointer').removeClass("current-read-pointer");
    },
    updatePointer: function() {
      this.cleanPointer();
      var element = $("#input-value-" + this.getIndex()).parent();
      element.addClass("current-read-pointer");
    },
    unselectCells: function() {
      this.m_body.find('.input-value').removeClass("used-input-cell-value");
      this.m_body.find('.input-index').removeClass("used-input-cell-index");
    },
    getCount: function() {
      return this.m_body.find('.input-index').length;
    },
    getLastCreatedIndex: function() {
      return this.m_body.find('.input-value').not('.blank-cell-value').length;
    },
    removeNewButton: function() {
      var index = this.m_body.find('.btn-input-new-value').parent().find('.blank-cell-index').attr('id');
      if (index !== undefined) {
        index = index.replace('input-index-', '');
        this.m_body.find('.btn-input-new-value').parent().append(
          this.createNewCellInput(index, '0', true)
        )
        this.m_body.find('.btn-input-new-value').remove();
      }
    },
    fixNewButton: function() {
      this.removeNewButton();

      var newButton = $('<buttons class="btn-input-new-value btn btn-block btn-outline-dark btn-lg mb-2 translate" data-toggle="tooltip" title="New input value" />').append(
        $('<i class="fas fa-plus" />')
      ).attr('translation-title-en', "New input value").on('click', this.onNewValue.bind(this));

      this.m_body.find('.blank-cell-value').first().parent().prepend(newButton).find('.blank-cell-value').remove();
      
      if (this.m_body.find('.blank-cell-value').length == 0) {
        this.createEmptyCellAt(this.getCount() +1);
      }
    },
    getIndex: function() {
      return this.m_currentIndex;
    },
    isNextBlank: function() {
      return $("#input-index-" + this.getIndex()).hasClass('blank-cell-index');
    },
    getValueAt: function(index, movePointer = true, highlight = true) {
      var element = $("#input-value-" + index);
      if (highlight) {
        element.addClass("used-input-cell-value");
        element.parent().find(".input-index").addClass("used-input-cell-index");
      }
      if (movePointer) {
        this.m_currentIndex++;
        this.updatePointer();
      }
      return element.val();
    },
    getNext: function(highlight = true) {
      var index = this.getIndex();
      if (!this.isNextBlank())
        return this.getValueAt(index, true, highlight);
      else
        return -1;
    },
    getPrev: function(highlight = false) {
      var index = this.getIndex() -1;
      if (index > 0)
        return this.getValueAt(index, false, highlight);
      else
        return -1;
    },
    newValue: function(value = 0, clicked = false, mustShowLoading = true) {
      if (mustShowLoading)
        this.showLoading();

      var index = parseInt(this.getLastCreatedIndex()) +1;
      var newVal = this.createNewCell(index, value);
      
      if ($("#input-index-" + index) === undefined)
        newVal.appendTo(this.m_body);
      else
        $("#input-index-" + index).parent().parent().empty().append(newVal.find(".input-container"));      

      if (clicked)
      {
        this.m_body.find('.input-value').not('.blank-cell-value').last().focus();
        this.m_body.find('.input-value').not('.blank-cell-value').last().select();
        
        this.fixTools();
      }

      this.fixNewButton();

      if (mustShowLoading)
        this.hideLoading();
    },
    createNewEditCell: function(value) {
      return $('<li />').append(
        $('<span class="handle" />').append(
          $('<i class="fas fa-grip-vertical" />')
        )
      ).append(
        $('<span class="text" />').text(this.fixValue(value))
      ).append(
        $('<div class="tools" />').append(
          $('<button class="btn btn-block translate" data-toggle="tooltip" title="'+translate("Remove")+'" />').append(
            $('<i class="fas fa-trash-alt" />')
          ).click(function() {
            $(this).parent().parent().remove();
          })
        )
      );
    },
    onModify: function() {
      var values = [];
      var inputObj = this;
      $.each(this.m_body.find(".input-index:visible").not('.blank-cell-index').parent(), function() {
        values.push(inputObj.fixValue($(this).find("input").val()));
      });

      this.startValuesEditing(values);
    },     
    startValuesEditing: function(values) {
      this.m_bIsEditing = true;
      this.showLoading();
      
      $.each(this.m_body.find(".input-index:visible").parent(), function() {
        $(this).hide();
      });

      $("#edit-input-buttons-container").show();
      
      var inputObj = this;
      inputObj.m_list.empty().show();

      $.each(values, function() {
        inputObj.m_list.append(inputObj.createNewEditCell(this));
      });
      
      this.fixTools();
      this.hideLoading();
    },
    stopValuesEditing: function(save = false) {
      this.showLoading();

      this.m_body.find(".blank-cell-index").parent().show();

      if (save)
        this.saveValuesEditing();
      else
        this.abortValuesEditing();
      
      this.m_list.empty().hide();
      $("#edit-input-buttons-container").hide();
      
      this.fixNewButton();
      this.fixTools();
      this.hideLoading();
      
      this.m_bIsEditing = false;
    },
    abortValuesEditing: function() {
      this.m_body.find(".input-value:not(visible)").parent().show();
    },
    saveValuesEditing: function() {
      var inputObj = this;

      this.createInitialEmptyCells();

      $.each(inputObj.m_body.find(".todo-list").find(".text"), function() {
        inputObj.newValue(inputObj.fixValue($(this).text()), false, false);
      });
    },
    onDoUpload: function() {
      if (typeof $('#file-input').prop('files')[0] !== 'undefined')
      {
        var inputObj = this;
        inputObj.showLoading(true);
        var fileReader = new FileReader();

        fileReader.onload = function() {
          var text = fileReader.result;
          inputObj.startValuesEditing(text.split('\n'));
          inputObj.m_body.find(".input-value:visible").not('.blank-cell-value').parent().hide();
          $("#file-input-modal").find(".close").trigger("click");
        };
        fileReader.onerror = function() {
          showError('Error', 'An error occurred uploading your file, please try again.');
        };
        fileReader.onloadend = function() {
          inputObj.hideLoading(true);
        };
    
        fileReader.readAsText($('#file-input').prop('files')[0]);
      }
    },
    showLoading: function(isUploading = false) {
      var element = this.m_body.parent();
      if (isUploading)
        element = $("#file-input-modal").find(".modal-content");
      element.append(
        $('<div class="overlay d-flex justify-content-center align-items-center dark" />').append('<i class="fas fa-2x fa-sync-alt fa-spin" />')
      );
    },
    hideLoading: function(isUploading = false) {
      var element = this.m_body.parent();
      if (isUploading)
        element = $("#file-input-modal").find(".modal-content");
      element.find(".overlay").remove();
    },
  };
  input.initialize();
  return input;
}
