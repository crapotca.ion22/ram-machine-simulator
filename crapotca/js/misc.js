function cleanOperandValue(operand) {
  operand = operand.toString();
  operand = operand.substr(1);
  return parseInt(operand);   
}

function isDirectOperandValue(operand) {
    operand = operand.toString();
    var firstChar = operand.charAt(0);
    return (firstChar == '=');
}

function isIndirectOperandValue(operand) {
    operand = operand.toString();
    var firstChar = operand.charAt(0);
    return (firstChar == '*');
}

function downloadFile(filename, content) {
    var a = document.createElement("a");
    a.href = 'data:text/plain;,' + encodeURIComponent(content);
    a.download = filename;
    document.body.appendChild(a);
    a.click();
    a.remove();
}

function showError(title, error, isBug = false, delay = 5000) {
    $(document).Toasts('create', {
      class: 'bg-danger', 
      title: translate(title),
      body: translate(error),
      icon: isBug ? 'fas fa-bug' : 'fa fa-skull-crossbones',
      delay: delay,
      autohide: true,
    });
}

function showWarning(title, warning, delay = 5000) {
    $(document).Toasts('create', {
        class: 'bg-warning', 
        title: translate(title),
        body: translate(warning),
        icon: 'fas fa-exclamation-triangle',
        delay: delay,
        autohide: true,
    });
}

String.prototype.format = String.prototype.f = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};