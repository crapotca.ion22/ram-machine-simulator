function initializeOutput() {
  var output = {
    DOWNLOAD_FILE_NAME: "output.txt",
    INITIAL_REGISTERS: 10,
    m_body: $("#output-body"),
    m_currentIndex: 1,
    m_btnDownload: $("#btn-output-download"),
    initialize: function() {
      this.bindEvents();
      this.resetOutput();
    },
    bindEvents: function() {
      this.m_btnDownload.on('click', this.onDownload.bind(this));
    },
    resetOutput: function() {
      this.m_currentIndex = 1;
      this.m_body.find(".output-value").parent().remove();
      for (var x = this.m_currentIndex; x < this.m_currentIndex + this.INITIAL_REGISTERS; x++) {
        this.createEmptyCellAt(x);
      }
      this.unselectCells();
      this.cleanPointer();
    },
    createEmptyCellAt: function(index) {
      var newEmpty = this.createNewCell(index, "", true);
      newEmpty.appendTo(this.m_body);
    },
    onDownload: function() {
      var values = [];
      $.each(this.m_body.find(".output-value"), function() {
        values.push($(this).val());
      });
    
      downloadFile(this.DOWNLOAD_FILE_NAME, values.join("\n"));
    },
    isBlank: function() {
      return $("#output-index-" + this.getIndex()).hasClass('blank-cell-index');
    },
    getCount: function() {
      return this.m_body.find('.output-value').length;
    },
    getIndex: function() {
      return this.m_currentIndex;
    },
    cleanPointer: function() {
      this.m_body.find('.current-write-pointer').removeClass("current-write-pointer");
    },
    updatePointer: function() {
      this.cleanPointer();
      var element = $("#output-value-" + this.getIndex()).parent();
      element.addClass("current-write-pointer");
    },
    unselectCells: function() {
      this.m_body.find('.output-value').removeClass("used-output-cell-value");
      this.m_body.find('.output-index').removeClass("used-output-cell-index");
    },
    createNewCell: function(index, value, isEmpty = false) {
      var cell =  $('<div class="row justify-content-center" />').append(
                $('<div class="col-sm-7" />').append(
                  $('<div class="output-container" />').append(
                    $('<input class="output-value form-control mb-2" type="text" />').prop("readonly", true).attr("id", "output-value-" + index).val(value)
                  ).append(
                    $('<span class="output-index badge badge-dark" />').attr("id", "output-index-" + index).text(index)
                  )
                )
              );

      if (isEmpty) {
        cell.find(".output-value").addClass("blank-cell-value").addClass("translate").val("Empty")
        cell.find(".output-index").addClass("blank-cell-index");
      }
      
      return cell;
    },
    newValue: function (value = 0, mustShowLoading = true, highlight = true) {
      if (mustShowLoading)
        this.showLoading();
    
      //this.m_emptyContainer.hide();
      var newVal = this.createNewCell(this.getIndex(), value);
      if ($("#output-index-" + this.getIndex()) === undefined)
        newVal.appendTo(this.m_body);
      else
        $("#output-index-" + this.getIndex()).parent().parent().empty().append(newVal.find(".output-container"));
      
      this.updatePointer();
      this.m_currentIndex++;
      
      if (this.isBlank()) {
        this.createEmptyCellAt(this.getIndex());
        this.updatePointer();
      }
      
      if (highlight) {
        var element = newVal.find(".output-value");
        element.addClass("used-output-cell-value");
        element.parent().find(".output-index").addClass("used-output-cell-index");
      }

      if (mustShowLoading)
        this.hideLoading();
    },
    showLoading: function() {
      this.m_body.parent().append(
        $('<div class="overlay d-flex justify-content-center align-items-center dark" />').append('<i class="fas fa-2x fa-sync-alt fa-spin" />')
      );
    },
    hideLoading: function() {
      this.m_body.parent().find(".overlay").remove();
    },
  };

  output.initialize();
  return output;
}
