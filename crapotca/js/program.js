function initializeProgram() {
  var program = {
    DOWNLOAD_FILE_NAME: "program.txt",
    m_body: $("#program-body"),
    m_editor: CodeMirror.fromTextArea(document.getElementById("textarea-program"), {
      lineNumbers: true,
      styleActiveLine: true,
      lineWrapping: true,
      viewportMargin: Infinity,
      tabSize: 2,
      indentUnit: 2,
      indentWithTabs: true,
      keyMap: "sublime",
      mode: {name: "ram_machine"},
      gutters: ["CodeMirror-lint-markers", "CodeMirror-linenumbers", "breakpoints"]
    }),
    m_btnClean: $("#btn-program-empty"),
    m_btnDownload: $("#btn-program-download"),
    m_btnExample1: $("#btn-program-example1"),
    m_btnExample2: $("#btn-program-example2"),
    m_btnExample3: $("#btn-program-example3"),
    m_btnExample4: $("#btn-program-example4"),
    m_btnDoUpload: $("#btn-program-upload-confirm"),
    initialize: function() {
      this.bindEvents();
      this.resetProgram();
      
      this.m_editor.setOption("lint", {editor: this.m_editor})
    },
    bindEvents: function() {
      this.m_btnDownload.on('click', this.onDownload.bind(this));
      this.m_btnClean.on('click', this.onClean.bind(this));
      this.m_btnExample1.on('click', this.onExample1.bind(this));
      this.m_btnExample2.on('click', this.onExample2.bind(this));
      this.m_btnExample3.on('click', this.onExample3.bind(this));
      this.m_btnExample4.on('click', this.onExample4.bind(this));
      this.m_editor.on('keyup', this.onEditorKeyUp.bind(this));
      this.m_editor.on('gutterClick', this.onBreakPointClick.bind(this));
      this.m_btnDoUpload.on('click', this.onDoUpload.bind(this));
    },
    resetProgram: function() {
      this.enableEditing();
    },
    onEditorKeyUp: function (cm, event) {
      var ch = String.fromCharCode(event.keyCode);
      //var excludedKeyCodes = [8, 9, 13, 16, 20, 17, 18, 32, 46];
      if (!cm.state.completionActive && /\S/.test(ch) /*&& excludedKeyCodes.indexOf(event.keyCode) < 0*/) {
        cm.showHint({
          completeSingle: false
        })
      }
    },
    createBreakPoint: function() {
      var marker = document.createElement("div");
      marker.innerHTML = '<i class="fas fa-arrow-alt-circle-right breakpoint translate" title="'+translate("Breakpoint")+'"></i>';
      return marker;
    },
    onBreakPointClick: function(cm, n) {
      var info = cm.lineInfo(n);
      cm.setGutterMarker(n, "breakpoints", info.gutterMarkers ? null : this.createBreakPoint());
    },
    getEditor: function() {
      return this.m_editor;
    },
    enableEditing: function() {
      this.m_body.find(".CodeMirror-lines").css("cursor", "");
      this.m_editor.setOption("styleActiveLine", true);
      this.m_editor.setOption("readOnly", false);
    },
    disableEditing: function() {
      this.m_body.find(".CodeMirror-lines").css("cursor", "not-allowed");
      this.m_editor.setOption("readOnly", "nocursor");
      this.m_editor.setOption("styleActiveLine", false);
    },
    onClean: function() {
      this.setProgram("");
    },
    onDownload: function() {
      downloadFile(this.DOWNLOAD_FILE_NAME, this.getProgram());
    },
    onExample1: function() {
      var program = [];
      program.push(translate("//Find the biggest among n numbers"));
      program.push("");
      program.push("READ 1");
      program.push("2 JBLANK 10");
      program.push("LOAD 1");
      program.push("READ 2");
      program.push("SUB 2");
      program.push("JGTZ 2");
      program.push("LOAD 2");
      program.push("STORE 1");
      program.push("JUMP 2");
      program.push("10 WRITE 1");
      program.push("HALT");

      this.setProgram(program.join("\n"));
    },
    onExample2: function() {
      var program = [];
      program.push(translate("//Sum of n numbers"));
      program.push("");
      program.push("1 JBLANK 5");
      program.push("READ 1");
      program.push("ADD 1");
      program.push("JUMP 1");
      program.push("5 WRITE 0");
      program.push("HALT");

      this.setProgram(program.join("\n"));
    },
    onExample3: function() {
      var program = [];
      program.push(translate("//Save n numbers in the registers R1...Rn"));
      program.push("");
      program.push("LOAD =1");
      program.push("2 JBLANK 6");
      program.push("READ *0");
      program.push("ADD =1");
      program.push("JUMP 2");
      program.push("6 HALT");

      this.setProgram(program.join("\n"));
    },
    onExample4: function() {
      var program = [];
      program.push(translate("//Calculate z = 3^2^n, having input n"));
      program.push("");
      program.push("READ 1");
      program.push("LOAD =3");
      program.push("STORE 2");
      program.push("LOAD 1");
      program.push("while JZERO endwhile");
      program.push("LOAD 2");
      program.push("MULT 2");
      program.push("STORE 2");
      program.push("LOAD 1");
      program.push("SUB =1");
      program.push("STORE 1");
      program.push("JUMP while");
      program.push("endwhile WRITE 2");
      program.push("HALT");

      this.setProgram(program.join("\n"));
    },
    onDoUpload: function() {
      if (typeof $('#file-program').prop('files')[0] !== 'undefined')
      {
        var programObj = this;
        programObj.showLoading(true);
        var fileReader = new FileReader();
        fileReader.onload = function() {
          var text = fileReader.result;
          programObj.setProgram(text);
          $("#file-program-modal").find(".close").trigger("click");
        };
        fileReader.onerror = function() {
          showError('Error', 'An error occurred uploading your file, please try again.');
        };
        fileReader.onloadend = function() {
          programObj.hideLoading(true);
        };
    
        fileReader.readAsText($('#file-program').prop('files')[0]);
      }
    },
    getProgram: function() {
      return this.m_editor.getValue();
    },
    setProgram: function(text) {
      this.showLoading();
      this.m_editor.setValue(text);
      this.hideLoading();
    },
    showLoading: function(isUploading = false) {
      var element = this.m_body.parent();
      if (isUploading)
        element = $("#file-program-modal").find(".modal-content");
      element.append(
        $('<div class="overlay d-flex justify-content-center align-items-center dark" />').append('<i class="fas fa-2x fa-sync-alt fa-spin" />')
      );
    },
    hideLoading: function(isUploading = false) {
      var element = this.m_body.parent();
      if (isUploading)
        element = $("#file-program-modal").find(".modal-content");
      element.find(".overlay").remove();
    },
  };

  program.initialize();
  return program;
}
