var g_machine = null;

$(document).ready(function () {
  bsCustomFileInput.init();

  g_machine = initializeMachine();
});

function initializeMachine() {
  var machine = {
    DEFAULT_STEPS_DELAY: 2,
    m_countDownValue: 0,
    m_currentStatus: "idle",
    m_bNextStep: false,
    m_currentBreakPointLine: -1,
    m_currentProgramLineIndex: -1,
    m_programLines: [],
    m_labelsPosMap: new Map(),
    m_timerVar: null,
    m_output: initializeOutput(),
    m_input: initializeInput(),
    m_program: initializeProgram(),
    m_register: initializeRegister(),
    m_history: initializeHistory(),
    m_cost: initializeCost(),
    m_status: $("#div-status"),
    m_timer: $("#status-timer"),
    m_btnPlay: $("#btn-play"),
    m_btnPause: $("#btn-pause"),
    m_btnReset: $("#btn-reset"),
    m_btnStep: $("#btn-step"),
    m_btnStop: $("#btn-stop"),
    m_slider: $("#slider").bootstrapSlider({
      lock_to_ticks: true
    }),
    initialize: function() {
      this.bindEvents();

      this.resetMachine();
      this.m_cost.passRegister(this.m_register);
      this.m_cost.passInput(this.m_input);
      
      this.m_register.passHistory(this.m_history);
      
      this.hideLoading($(".overlay").parent());

      var stepsDelay = Cookies.get('stepsDelay');
      if (stepsDelay === undefined)
        stepsDelay = this.DEFAULT_STEPS_DELAY;
      this.m_slider.bootstrapSlider('setValue', stepsDelay);
    },
    bindEvents: function() {
      this.m_btnPlay.on('click', this.onPlay.bind(this));
      this.m_btnPause.on('click', this.onPause.bind(this));
      this.m_btnReset.on('click', this.onReset.bind(this));
      this.m_btnStep.on('click', this.onStep.bind(this));
      this.m_btnStop.on('click', this.onStop.bind(this));
      this.m_slider.on('slideStop', this.onStepDelayStopped.bind(this));
    },
    showLoading: function(element) {
      element.append(
        $('<div class="overlay d-flex justify-content-center align-items-center dark" />').append('<i class="fas fa-2x fa-sync-alt fa-spin" />')
      );
    },
    hideLoading: function(element) {
      element.find(".overlay").remove();
    },
    getStepsDelay: function() {
      return this.m_slider.bootstrapSlider('getValue');
    },
    updateTimerCountDown: function() {
      var machine = g_machine;

      if (machine.getCurrentStatus() == "running") {
        machine.m_timer.text(machine.m_countDownValue);
        machine.m_countDownValue--;
  
        var timerIcon = machine.m_timer.parent().find("i");
        switch (timerIcon.attr("class")) {
          case "fas fa-hourglass-start":
            timerIcon.attr("class", "fas fa-hourglass-half");
            break;
          case "fas fa-hourglass-half":
            timerIcon.attr("class", "fas fa-hourglass-end");
            break;
          case "fas fa-hourglass-end":
            timerIcon.attr("class", "fas fa-hourglass-start");
            break;
        }
        
        if (machine.m_countDownValue < 0)
          machine.fetchDecodeExecute();
      }
    },
    resetMachine: function() {
      this.m_currentProgramLineIndex = -1;
      this.m_currentBreakPointLine = -1;
      this.m_bNextStep = false;
      this.m_labelsPosMap = new Map();
      this.m_programLines = [];
      this.m_countDownValue = this.getStepsDelay();

      this.m_register.resetRegisters();
      this.m_input.resetInput();
      this.m_output.resetOutput();
      this.m_program.resetProgram();
      this.m_history.resetHistory();
      this.m_cost.resetCost();
      
      this.m_btnPlay.find("span").text(translate("Run"));
      this.changeStatus("idle");
      
      this.m_input.fixTools();
      $(".hideWhenRunning").show();
      $(".showWhenRunning").hide();
      
      $(".CodeMirror-code").children().removeClass("CodeMirror-used-line");
      $(".CodeMirror-code").children().removeClass("current-location-pointer");
    },
    getCurrentStatus: function() {
      return this.m_currentStatus;
    },
    changeStatus: function(status) {
      this.m_currentStatus = status;

      switch(status) {
        case "idle":
          this.m_status.hide();
  
          this.m_btnReset.hide();
          this.m_btnPause.hide();
          this.m_btnStop.hide();

          this.m_btnPlay.show();
          this.m_btnStep.show();
          break;
        case "running":
          this.m_status.find(".card-title i").removeAttr("class").addClass("fas fa-play");
          this.m_status.find(".card-title span").text(translate("Running"));
          this.m_status.show();

          this.m_btnPlay.hide();
          this.m_btnReset.hide();
          this.m_btnStep.hide();

          this.m_btnPause.show();
          this.m_btnStop.show();
          break;
        case "pause":
          this.m_status.find(".card-title i").removeAttr("class").addClass("fas fa-pause");
          this.m_status.find(".card-title span").text(translate("Paused"));
          this.m_status.show();

          this.m_btnPlay.show();
          this.m_btnReset.hide();
          this.m_btnPause.hide();

          this.m_btnStep.show();
          this.m_btnStop.show();

          if (this.m_timerVar != null) {
            clearInterval(this.m_timerVar);
            this.m_timerVar = null;
          }
        break;
        case "breakpoint":
          this.m_status.find(".card-title i").removeAttr("class").addClass("fas fa-arrow-alt-circle-right");
          this.m_status.find(".card-title span").text(translate("Breakpoint"));
          this.m_status.show();

          this.m_btnPlay.show();
          this.m_btnReset.hide();
          this.m_btnPause.hide();

          this.m_btnStep.show();
          this.m_btnStop.show();

          this.m_countDownValue = 1;
          this.updateTimerCountDown();
        break;
        case "stop":
          this.m_status.find(".card-title i").removeAttr("class").addClass("fas fa-stop");
          this.m_status.find(".card-title span").text(translate("Stopped"));
          this.m_status.show();
  
          this.m_btnPause.hide();
          this.m_btnStop.hide();
          this.m_btnPlay.hide();
          this.m_btnStep.hide();

          this.m_btnReset.show();
        break;
        case "abort":
          this.m_status.find(".card-title i").removeAttr("class").addClass("fas fa-heart-broken");
          this.m_status.find(".card-title span").text(translate("Interrupted"));
          this.m_status.show();
  
          this.m_btnPause.hide();
          this.m_btnStop.hide();
          this.m_btnPlay.hide();
          this.m_btnStep.hide();

          this.m_btnReset.show();
          this.m_status.effect("shake").effect("highlight", {color:"red"}, 1000);

          this.m_cost.infinite();
        break;
      }
    },
    canRunProgram: function() {
      if (this.m_input.isEditing()) {
        showWarning('Warning', 'Please finish editing the Read Tape before running the program.');
        $.each(this.m_input.m_body.parent().children(), function() {
          $(this).effect("shake").effect("highlight", {color:"yellow"}, 1000);
        });
        return false;
      }

      var errorsFound = $("#textarea-program").parent().find(".CodeMirror-lint-marker-error").length > 0;
      if (errorsFound)
      {
        showError('Error', 'The program contains errors, please fix them and retry.', true);
        $.each(this.m_program.m_body.parent().children(), function() {
          $(this).effect("shake").effect("highlight", {color:"red"}, 1000);
        });
        return false;
      }

      return true;
    },
    startExecution: function() {
      $(".hideWhenRunning").hide();
      $(".showWhenRunning").show();
      this.m_input.fixAllValues();
      this.m_input.disableEditing();
      this.m_input.updatePointer();
      this.m_output.updatePointer();
      this.m_program.disableEditing();
      this.mapProgramInstructions();
      this.m_btnPlay.find("span").text(translate("Continue"));
      this.fetchDecodeExecute();
    },
    mapProgramInstructions: function() {
      var editor = this.m_program.getEditor();
      var labelsMap = this.m_labelsPosMap;
      var programLines = this.m_programLines;

      for (var x = 0; x < editor.lineCount(); x++)
      {
        var programLine = {
          label: null,
          opcode: null,
          operand: null,
          line: x
        };
        
        $.each(editor.getLineTokens(x), function() {
          var token = this;
          if (token.type != null && token.type != "comment")
          {
            switch (token.type) {
              case "label":
                if (programLine.opcode == null) {
                  programLine.label = token.string;
                  break;
                }
              case "operand":
              case "operand-2":
              case "operand-3":
                programLine.operand = token.string;
                break;
              case "opcode":
                programLine.opcode = token.string.toUpperCase();
                break;
            }
          }
        });
        
        if (programLine.label != null || programLine.opcode != null)
        {
          if (programLine.label != null)
            labelsMap.set(programLine.label, programLines.length);
          programLines.push(programLine);
        }
      }
    },
    getOperandValue: function(operand) {
      if (isDirectOperandValue(operand)) {
        operand = cleanOperandValue(operand);
        return operand;
      }
      if (isIndirectOperandValue(operand)) {
        operand = cleanOperandValue(operand);
        if (operand < 0) {
          this.changeStatus("abort");
          showError('Error', 'The registry index can\'t be less than zero.', true, 10000);
        }
        var highlight = this.getStepsDelay() != 0 || this.m_bNextStep;

        var oldOp = operand;
        operand = this.m_register.getValue(operand, (highlight) ? this.m_register.HIGHLIGHT_TYPE.INDIRECT : this.m_register.HIGHLIGHT_TYPE.NONE); 
        this.m_history.addIndirectRegister(oldOp, this.m_register.getValue(oldOp), operand, this.m_register.getValue(operand));
      }
      if (operand < 0) {
        this.changeStatus("abort");
        showError('Error', 'The registry index can\'t be less than zero.', true, 10000);
      }
      operand = this.m_register.getValue(operand);
      return operand;
    },
    getOperandIndex: function(operand) {
      if (isDirectOperandValue(operand)) {
        this.changeStatus("abort");
        showError('Error', 'Can\'t use a direct operand value (=i) here.', true, 10000);
      }
      if (isIndirectOperandValue(operand)) {
        operand = cleanOperandValue(operand);
        var highlight = this.getStepsDelay() != 0 || this.m_bNextStep;
        if (operand < 0) {
          this.changeStatus("abort");
          showError('Error', 'The registry index can\'t be less than zero.', true, 10000);
        }
        var oldOp = operand;
        operand = this.m_register.getValue(operand, (highlight) ? this.m_register.HIGHLIGHT_TYPE.INDIRECT : this.m_register.HIGHLIGHT_TYPE.NONE); 
        this.m_history.addIndirectRegister(oldOp, this.m_register.getValue(oldOp), operand, this.m_register.getValue(operand));
      }
      return operand;
    },
    fetchDecodeExecute: function() {
      if (this.getCurrentStatus() == "running" || this.m_bNextStep) {
        var countDownValue = 0;
        if (!this.m_bNextStep)
          countDownValue = this.getStepsDelay();
        if (this.m_countDownValue <= 0)
          this.m_countDownValue = countDownValue;
        if (countDownValue > 0)
          this.updateTimerCountDown();

        var highlight = countDownValue > 0 || this.m_bNextStep;

        if (this.m_timerVar != null || countDownValue <= 0) {
          var timerIcon = machine.m_timer.parent().find("i");
          timerIcon.attr("class", "fas fa-hourglass-start");
          if (this.m_timerVar != null) {
            clearInterval(this.m_timerVar);
            this.m_timerVar = null;
          }

          var lastLabel = null;
          var instruction = {
            label: null,
            opcode: null,
            operand: null,
            line: 0
          };

          while (instruction.opcode == null) {
            $(".CodeMirror-code").children().removeClass("CodeMirror-used-line");
            $(".CodeMirror-code").children().removeClass("CodeMirror-breakpointline");
            $(".CodeMirror-code").children().removeClass("current-location-pointer");
            this.m_currentProgramLineIndex++;

            instruction = this.m_programLines[this.m_currentProgramLineIndex];
            
            if (instruction === undefined) {
              var lastInstruction = this.m_programLines[this.m_currentProgramLineIndex-1];
              var line = (lastInstruction === undefined) ? 0 : lastInstruction.line;

              instruction = {
                label: null,
                opcode: "HALT",
                operand: null,
                line: line +1
              };

              this.m_programLines.push(instruction);
              
              this.m_program.getEditor().replaceRange("\n" + instruction.opcode, CodeMirror.Pos(line));
            }
            
            if (this.m_program.getEditor().lineInfo(instruction.line).gutterMarkers) {
              if (instruction.line != this.m_currentBreakPointLine)
              {
                $(".CodeMirror-code").children().eq(instruction.line).addClass("CodeMirror-breakpointline");
                this.m_currentBreakPointLine = instruction.line;
                this.m_currentProgramLineIndex--;
                this.changeStatus("breakpoint");
                break;
              }
              else
              {
                this.m_currentBreakPointLine = -1;
              }
            }

            if (instruction.label == null && lastLabel != null)
            {
              instruction.label = lastLabel;
              lastLabel = null;
            }

            if (instruction.opcode != null)
            {
              this.m_history.newValue(instruction);
              
              $(".CodeMirror-code").children().eq(instruction.line).addClass("CodeMirror-used-line");
              
              if (this.m_history.getCount() == 1)
                this.m_register.updateLocationCounter(this.m_programLines[this.m_currentProgramLineIndex].line +1, false, highlight);
    
              this.m_register.unselectCells();
              this.m_input.unselectCells();
              this.m_output.unselectCells();

              switch (instruction.opcode) {
              //JUMP
                case "JUMP":
                  this.jumpToLabel(instruction.operand);
                break;
                case "JZERO":
                  if (this.m_register.getValue(0, (highlight) ? this.m_register.HIGHLIGHT_TYPE.SELECTED : this.m_register.HIGHLIGHT_TYPE.NONE) == 0)
                    this.jumpToLabel(instruction.operand);
                break;
                case "JGTZ":
                  if (this.m_register.getValue(0, (highlight) ? this.m_register.HIGHLIGHT_TYPE.SELECTED : this.m_register.HIGHLIGHT_TYPE.NONE) > 0)
                    this.jumpToLabel(instruction.operand);
                break;
                case "JBLANK":
                  if (this.m_input.isNextBlank())
                    this.jumpToLabel(instruction.operand);
                break;
              //MOVE
                case "LOAD":
                  var operand = this.getOperandValue(instruction.operand);
                  this.m_register.setValue(0, operand, highlight);
                break;
                case "STORE":
                  var index = this.getOperandIndex(instruction.operand);
                  this.m_register.setValue(index, this.m_register.getValue(0), highlight);
                break;
              //MATH
                case "ADD":
                  var operand = this.getOperandValue(instruction.operand);
                  this.m_register.setValue(0, this.m_register.getValue(0) + operand, highlight);
                break;
                case "SUB":
                  var operand = this.getOperandValue(instruction.operand);
                  this.m_register.setValue(0, this.m_register.getValue(0) - operand, highlight);
                break;
                case "MULT":
                  var operand = this.getOperandValue(instruction.operand);
                  this.m_register.setValue(0, this.m_register.getValue(0) * operand, highlight);
                break;
                case "DIV":
                  var operand = this.getOperandValue(instruction.operand);
                  //check for div by zero
                  if (operand == "0") {
                    this.changeStatus("abort");
                    showError('Error', 'Division by zero.', true, 10000);
                  }
                  this.m_register.setValue(0, this.m_register.getValue(0) / operand, highlight);
                break;
              //IO
                case "READ":
                  if (this.m_input.isNextBlank()) {
                    this.changeStatus("abort");
                    showError('Error', 'Can\'t read a value from the input tape because it\'s blank.', true, 10000);
                  }
                  else
                  {
                    var index = this.getOperandIndex(instruction.operand);
                    this.m_register.setValue(index, this.m_input.getNext(highlight), highlight);
                    this.m_register.updateReadCounter(this.m_input.getIndex(), highlight);
                  }
                break;
                case "WRITE":
                  var operand = this.getOperandValue(instruction.operand);
                  this.m_output.newValue(operand, true, highlight);
                  this.m_register.updateWriteCounter(this.m_output.getIndex(), highlight);
                break;
                case "HALT":
                  this.changeStatus("stop");
                  break;
                default:
                  this.changeStatus("abort");
                  showError('Error', translate('The op. code "{0}" is not implemented yet.').format(instruction.opcode), true, 10000);
              }

              if (instruction.opcode != "HALT") {
                var lineIndex = this.m_currentProgramLineIndex;
                if (instruction.opcode == "JUMP")
                  lineIndex++;
                var line = this.m_programLines[lineIndex].line +1;
                if (instruction.opcode == "JUMP")
                  line--;

                $(".CodeMirror-code").children().eq(line).addClass("current-location-pointer");
                this.m_register.updateLocationCounter(line +1, true, highlight);
              }

              this.m_cost.update(instruction);
            }
            else
            {
              lastLabel = instruction.label;
            }
          }
        }
        
        this.m_bNextStep = false;

        if (this.getCurrentStatus() == "running") {
          if (countDownValue > 0)
            this.m_timerVar = setInterval(this.updateTimerCountDown, 1000);
          else
            this.fetchDecodeExecute();
        }
      }
    },
    jumpToLabel(label) {
      if (!this.m_labelsPosMap.has(label))
      {
        this.changeStatus("abort");
        showError('Error', translate('The label "{0}" is not defined in the program.').format(label), true, 10000);
      }
      else
      {
        this.m_currentProgramLineIndex = this.m_labelsPosMap.get(label) -1;
      }
    },
    onPlay: function() {
      if (this.getCurrentStatus() == "idle")
      {
        this.m_program.showLoading();
        var canRun = this.canRunProgram();
        this.m_program.hideLoading();

        if (canRun) {
          this.changeStatus("running");
          this.m_countDownValue = this.getStepsDelay();
          this.startExecution();
        }
      }
      else
      {
        this.changeStatus("running");
        this.fetchDecodeExecute();
      }
    },
    onStep: function() {
      if (this.getCurrentStatus() == "idle")
      {
        this.m_program.showLoading();
        var canRun = this.canRunProgram();
        this.m_program.hideLoading();

        if (canRun) {
          this.changeStatus("pause");
          this.m_bNextStep = true;
          this.startExecution();
        }
      }
      else
      {
        this.changeStatus("pause");
        this.m_bNextStep = true;
        this.fetchDecodeExecute(true);
      }
    },
    onPause: function() {
      this.changeStatus("pause");
    },
    onReset: function() {
      this.resetMachine();
    },
    onStop: function() {
      this.changeStatus("stop");
    },
    onStepDelayStopped: function() {      
      Cookies.set('stepsDelay', this.getStepsDelay(), { expires: 365, sameSite: 'strict' });
    }
  }
  machine.initialize();
  return machine;
}