function initializeRegister() {
  var register = {
    INITIAL_REGISTERS: 10,
    HIGHLIGHT_TYPE: {
      NONE: 0,
      SELECTED: 1,
      INDIRECT: 2
    },
    COLOR_UPDATED: "lime",
    TIME_UPDATE: 250,
    m_history: null,
    m_body: $("#registers-body"),
    m_switchCounters: $("#switchCounters"),
    m_countersContainer: $("#counters-container"),
    initialize: function() {
      this.bindEvents();
      this.createMissingCells(this.INITIAL_REGISTERS);        
      this.resetRegisters();

      var showCounters = Cookies.get('showCounters');
      if (showCounters !== undefined)
      {
        this.m_switchCounters.prop("checked", (showCounters === 'true'));
        this.hideShowCounters();
      }
    },
    bindEvents: function() {
      this.m_switchCounters.on('click', this.onSwitchClick.bind(this));
    },
    passHistory: function(history) {
      this.m_history = history;
    },
    resetRegisters: function() {
      this.changeValue($(".counter-value"), "1");
      this.changeValue($(".registers-value"), "0");
      this.unselectCells();
      this.hideShowCounters();
    },
    onSwitchClick: function() {
      this.hideShowCounters();
    },
    hideShowCounters: function() {
      var showCounters = this.m_switchCounters.is(":checked");      

      if (showCounters)
        this.m_countersContainer.show();
      else
        this.m_countersContainer.hide();
        
      Cookies.set('showCounters', showCounters, { expires: 365, sameSite: 'strict' });
    },
    createMissingCells: function(requestedCellIndex) {
      this.showLoading();
      for (var x = this.getCount(); x < requestedCellIndex; x++)
      {
        this.newValue(0, false);
      }
      this.hideLoading();
    },
    setValue: function(index, value, highlight = true) {
      this.createMissingCells(parseInt(index) +1);
      
      var element = this.m_body.find("#registers-value-" + index);
      if (highlight) {
        element.addClass("used-register-cell-value");
        element.parent().find(".registers-index").addClass("used-register-cell-index");
      }

      var oldVal = this.getValue(index);

      this.changeValue(element, value, highlight);
      this.m_history.addRegisterChange(index, oldVal, value);
    },
    changeValue: function(element, value, highlight = false) {
      element.val(value);
      if (highlight)
        element.effect("highlight", {color: this.COLOR_UPDATED}, this.TIME_UPDATE);
    },
    getLocationCounter: function() {
      return $("#counter-value-location").val();
    },
    updateLocationCounter: function(value = -1, addToHistory = true, highlight = true) {
      var oldLc = this.getLocationCounter();

      var element = $("#counter-value-location");
      element.val(value);
      if (highlight) {
        element.addClass("used-location-counter-value");
        element.parent().find(".counter-index").addClass("used-location-counter-index");
        element.effect("highlight", {color: this.COLOR_UPDATED}, this.TIME_UPDATE);
      }
      
      var newLc = this.getLocationCounter();
      if (addToHistory)
        this.m_history.addLocationCounterChange(oldLc, newLc);
    },
    getReadCounter: function() {
      return $("#counter-value-read").val();
    },
    updateReadCounter: function(value = -1, highlight = true) {
      var oldRc = this.getReadCounter();

      var element = $("#counter-value-read");
      element.val(value);
      if (highlight) {
        element.addClass("used-read-counter-value");
        element.parent().find(".counter-index").addClass("used-read-counter-index");
        element.effect("highlight", {color: this.COLOR_UPDATED}, this.TIME_UPDATE);
      }

      var newRc = this.getReadCounter();
      this.m_history.addReadCounterChange(oldRc, newRc);
    },
    getWriteCounter: function() {
      return $("#counter-value-write").val();
    },
    updateWriteCounter: function(value = -1, highlight = true) {
      var oldWc = this.getWriteCounter();

      var element = $("#counter-value-write");
      element.val(value);
      if (highlight) {
        element.addClass("used-write-counter-value");
        element.parent().find(".counter-index").addClass("used-write-counter-index");
        element.effect("highlight", {color: this.COLOR_UPDATED}, this.TIME_UPDATE);
      }

      var newWc = this.getWriteCounter();
      this.m_history.addWriteCounterChange(oldWc, newWc);
    },
    unselectCells: function() {
      this.m_body.find('.registers-value').removeClass("used-register-cell-value");
      this.m_body.find('.registers-value').removeClass("used-register-cell-value-indirect");
      this.m_body.find('.registers-index').removeClass("used-register-cell-index");
      this.m_body.find('.registers-index').removeClass("used-register-cell-index-indirect");

      this.m_body.find('.counter-value').removeClass("used-location-counter-value");
      this.m_body.find('.counter-index').removeClass("used-location-counter-index");
      this.m_body.find('.counter-value').removeClass("used-read-counter-value");
      this.m_body.find('.counter-index').removeClass("used-read-counter-index");
      this.m_body.find('.counter-value').removeClass("used-write-counter-value");
      this.m_body.find('.counter-index').removeClass("used-write-counter-index");
    },
    getCount: function() {
      return this.m_body.find('.registers-index').length;
    },
    getValue(index, highlight = this.HIGHLIGHT_TYPE.NONE) {
      this.createMissingCells(parseInt(index) +1);

      var element = this.m_body.find("#registers-value-" + index);

      switch (highlight) {
        case this.HIGHLIGHT_TYPE.SELECTED:
          element.addClass("used-register-cell-value");
          element.parent().find(".registers-index").addClass("used-register-cell-index");
        break;
        case this.HIGHLIGHT_TYPE.INDIRECT:
          element.addClass("used-register-cell-value-indirect");
          element.parent().find(".registers-index").addClass("used-register-cell-index-indirect");
        break;
      }
      return parseInt(element.val());
    },
    createNewCell: function(index, value) {
      return  $('<div class="row justify-content-center" />').append(
                $('<div class="col-sm-7" />').append(
                  $('<div class="registers-container" />').append(
                    $('<input class="registers-value form-control mb-2" type="text" />').prop("readonly", true).attr("id", "registers-value-" + index).val(value)
                  ).append(
                    $('<span class="registers-index badge badge-dark" />').text("R" + index)
                  )
                )
              );
    },
    newValue: function(value = 0, mustShowLoading = true) {
      if (mustShowLoading)
        this.showLoading();

      var index = this.getCount();        
      var newCell = this.createNewCell(index, value);
      newCell.appendTo(this.m_body);
      
      if (mustShowLoading)
        this.hideLoading();
    },
    showLoading: function() {
      var element = this.m_body.parent();
      element.append(
        $('<div class="overlay d-flex justify-content-center align-items-center dark" />').append('<i class="fas fa-2x fa-sync-alt fa-spin" />')
      );
    },
    hideLoading: function() {
      var element = this.m_body.parent();
      element.find(".overlay").remove();
    },
  };
  register.initialize();
  return register;
}
