var DEFAULT_LANGUAGE = 'it';
var dictionary = {
    it : {
        'RAM Machine Simulator' : 'Simulatore Macchina RAM',
        'Program' : 'Programma',
        'Clean' : 'Svuota',
        'Upload' : 'Carica',
        'Download' : 'Scarica',
        'Examples' : 'Esempi',
        'Example 1' : 'Esempio 1',
        'Example 2' : 'Esempio 2',
        'Example 3' : 'Esempio 3',
        'Example 4' : 'Esempio 4',
        'Source code goes here...' : 'Il codice sorgente va messo qui...',
        'Read tape' : 'Nastro di scrittura',
        'Modify' : 'Modifica',
        'New input value' : 'Nuovo valore di input',
        'Abort changes' : 'Annulla le modifiche',
        'Save changes' : 'Salva le modifiche',
        'Blank' : 'Vuoto',
        'Registers' : 'Registri',
        'Show counter' : 'Mostra contatore',
        'R' : 'L',
        'W' : 'S',
        'LC' : 'CL',
        'Write tape' : 'Nastro di scrittura',
        'Empty' : 'Vuoto',
        'Run' : 'Avvia',
        'Step' : 'Passo',
        'Pause' : 'Pausa',
        'Stop' : 'Ferma',
        'Reset' : 'Resetta',
        'Steps delay (in seconds)' : 'Tempo tra i passi (in secondi)',
        'Call Stack' : 'Storico',
        'Show changes' : 'Mostra modifiche',
        'Changes' : 'Modifiche',
        'Uniform Cost' : 'Costo Uniforme',
        'Logarithmic Cost' : 'Costo Logaritmico',
        'Time' : 'Tempo',
        'Space' : 'Spazio',
        'Line' : 'Linea',
        'Label' : 'Etichetta',
        'Op. Code' : 'Codice Op.',
        'Operand' : 'Operando',
        'Select a file to fill the read tape' : 'Seleziona il file con cui riempire il nastro di lettura',
        'Choose input file' : 'Scegli il file di input',
        'Each line of the file must have an integer value' : 'Ogni linea del file deve avere un valore intero',
        'Cancel' : 'Annulla',
        'Select a source file' : 'Seleziona il file con il sorgente',
        'Choose source file' : 'Scegli il file con il sorgente',
        'University of Insubria' : 'Università degli Studi dell\'Insubria',
        'First cycle degree in computer science' : 'Laurea triennale in Informatica',
        'Source Code' : 'Codice Sorgente',
        'English' : 'Inglese',
        'Italian' : 'Italiano',
        'Running' : 'Sta girando',
        'Paused' : 'In pausa',
        'Breakpoint' : 'Punto di interruzione',
        'Stopped' : 'Fermato',
        'Interrupted' : 'Interrotto',
        'Continue' : 'Continua',
        'The op. code "{0}" is not implemented yet.' : 'Il codice op. "{0}" non è ancora stato implementato.',
        'The label "{0}" is not defined in the program.' : 'L\'etichetta "{0}" non è definita nel programma.',
        'Error' : 'Errore',
        'Warning' : 'Attenzione',
        'The program contains errors, please fix them and retry.' : 'Il programma contiene degli errori, per favore risolvili e riprova.',
        'The registry index can\'t be less than zero.' : 'L\'indirizzo del registro non può essere minore di zero.',
        'Can\'t use a direct operand value (=i) here.' : 'Non si può usare un operatore con valore diretto (=i) qui.',
        'Division by zero.' : 'Divisione per zero.',
        'Can\'t read a value from the input tape because it\'s blank.' : 'Non è possibile leggere un valore dal nastro di lettura perchè è vuoto.',
        'Please finish editing the Read Tape before running the program.' : 'Per favore finisci di editare il Nastro di lettura prima di avviare il programma.',
        'An error occurred uploading your file, please try again.' : 'Si è verificato un errore caricando il tuo file, riprova per favore.',
        'Remove' : 'Rimuovi',
        '//Find the biggest among n numbers' : '//Calcolo del massimo tra n numeri',
        '//Sum of n numbers' : '//Somma di n numeri',
        '//Save n numbers in the registers R1...Rn' : '//Memorizzazione di n numeri nei registri R1...Rn',
        '//Calculate z = 3^2^n, having input n' : '//Calcolo di z = 3^2^n, su input n',
    }
}

var m_observer = new MutationObserver(function(mutations) {
    mutations.forEach(function(mutation) {        
        $(mutation.addedNodes).find(".translate").each(function() {
            translateElement($(this));
        });
        
        $(mutation.target).find(".translate").each(function() {
            translateElement($(this));
        });
    });
});

function translateItem(element, lang, what) {    
    if (element.attr('translation-' + what +'-en') === undefined) {
        var eng = '';
        switch (what) {
            case 'text':
                eng = element.text().trim();
            break;
            case 'value':
                eng = element.val().trim();
            break;
            case 'title':
            case 'tooltip':
            case 'placeholder':
                if (element.attr(what) !== undefined)
                    eng = element.attr(what).trim();
        }
        if (eng.length > 0)
            element.attr('translation-' + what +'-en', eng);
    }

    var eng = element.attr('translation-' + what +'-en');
    if (element.attr('translation-' + what +'-en') !== undefined) {
        $.tr.language(lang, true);
        var tr = $.tr.translator();
        var translation = tr(eng);
        switch (what) {
            case 'text':
                element.text(translation);
            break;
            case 'value':
                element.val(translation);
            break;
            case 'title':
            case 'tooltip':
            case 'placeholder':
                element.attr(what, translation);
        }
    }
}

function translateElement(item, lang) {    
    translateItem(item, lang, 'text');
    translateItem(item, lang, 'value');
    translateItem(item, lang, 'title');
    translateItem(item, lang, 'tooltip');
    translateItem(item, lang, 'placeholder');
}

function translatePage(lang) {
    $.each($('.translate'), function() {
        translateElement($(this), lang);
    })
}

function getLanguage() {
    return $('#language-menu .dropdown-item.active').attr("language");
}

function doTranslation() {
    m_observer.disconnect();

    var language = getLanguage();
    Cookies.set('language', language, { expires: 365, sameSite: 'strict' });
    $.tr.language(language, true);
    translatePage(language);
    
    m_observer.observe(document, { attributes: true, childList: true,  subtree: true });    
}

function translate(text) {
    var tr = $.tr.translator();
    var translation = tr(text);
    return translation;
}

function setupTranslation() {
    $.tr.dictionary(dictionary);

    var language = Cookies.get('language');
    if (language === undefined)
        language = DEFAULT_LANGUAGE;

    $.tr.language(language, true);
    
    $('#language-menu .dropdown-item[language="'+language+'"]').trigger("click");
}

$(document).ready(function() {
    $(".CodeMirror-placeholder").addClass("translate");

    $('#language-menu .dropdown-item').on("click", function() {
        $.each($('#language-menu .dropdown-item'), function() {
            $(this).removeClass("active");
        })
        $(this).addClass("active");

        $(this).find("i").removeClass("mr-2");
        $("#language-preview i").attr("class", $(this).find("i").attr("class"));
        $(this).find("i").addClass("mr-2");
        
        doTranslation();

        $("#language-preview").attr("title", $(this).find("span").text());
    })
    
    setupTranslation();
    
});